# **lsi_dep** #

Command-line tool to manage packages dependencies in LSI code repositories.

### How does it work? ###

**lsi_dep** will find all repositories (modules) located in your workspace (default to "*~/catkin_ws/src*") and check their dependencies.

Each module's dependencies must be specified in a yaml file named "*.lsimodules*", located in the module's root directory. An example of this file can be found [here](https://bitbucket.org/Butakus/lsi_dep/src/7d3b3fef2fd680d5fe81ee2ec2531b390d4404fe/.lsimodules?at=master&fileviewer=file-view-default).

All dependencies will be downloaded in the submodules' directory (default to "*~/catkin_ws/src/modules*"). After the download it is possible to move them across the workspace and organize them in folders.

### Installation ###

```
#!bash

git clone git@bitbucket.org:Butakus/lsi_dep.git
sudo ./install.sh
```
The python script will be copied to "*/usr/local/bin*". The repository can be removed after the installation.

### Usage ###

To see how to use **lsi_dep** and check all options run:
```
#!bash
lsi_dep -h
```

**lsi_dep** help output:
```
#!text
usage: lsi_dep <command> [<options>]

LSI dependencies manager.

commands:
  init                 Check and download dependencies
  update               Update submodules repositories
  status               Check the changes in the repositories

options:
  -w, --workspace <src_path>        Packages workspace
                                    (Default: ~/catkin_ws/src)
  -p, --modules-path <modules_path> Path to install submodules
                                    (Default: ~/catkin_ws/src/modules)
  -f, --force-pull                  Update repositories without checking changes
  --https                           Convert all repositories URLs to HTTPS
  -v, --version                     Print version and exit
  -h, --help                        Print this text and exit

```

Example: Check all dependencies of modules installed in "*~/catkin_ws/src/lsi*" and download them in "*~/catkin_ws/src/lsi/submodules*"
```
#!bash
lsi_dep init -w ~/catkin_ws/src/lsi -p ~/catkin_ws/src/lsi/submodules
```
Example: Update (pull) all modules in "*~/catkin_ws/src/lsi*"
```
#!bash
lsi_dep update -p ~/catkin_ws/src/lsi/
```

*Note:* The **update** command is meant to update the dependencies, so in order to update all our packages we need to set the submodules path to "*lsi*" and not "*lsi/submodules*".


### TODOs ###
* Add packages list or exclusion mechanism to avoid updating all modules

### Notes ###
It is encouraged to use SSH URLs over HTTPS, as explained [here](https://confluence.atlassian.com/bitbucket/use-the-ssh-protocol-with-bitbucket-cloud-221449711.html).

You can learn how to generate the RSA keys and set up SSH with Bitbucket [here](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html).

If the HTTPS mode is prefered, or the user has not set the SSH keys, the option --https can be passed to *lsi_dep*, so it will convert all SSH URLs to the HTTPS format.

### Contact ###
If you find any problem, or you want to suggest a new feature, please open a new [issue](https://bitbucket.org/Butakus/lsi_dep/issues) or email me at franmore[at]ing.uc3m.es.