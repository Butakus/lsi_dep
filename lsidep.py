#!/usr/bin/python2

# Copyright (C) 2016  Francisco Miguel Moreno Olivo
# Email: franmore@ing.uc3m.es
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" TODO:
- Add packages list or exclusion mechanism to avoid updating all modules
"""

import os
import subprocess
import yaml
import argparse
from argparse import ArgumentParser
from textwrap import dedent


WORKSPACE = os.path.expanduser("~/catkin_ws/src")
MODULES_PATH = os.path.expanduser("~/catkin_ws/src/modules")

FORCE_PULL = False
FORCE_REINIT = False
FORCE_HTTPS = False

apt_packages = []

class Color:
    """ ANSI Colors """
    red = '\033[91m'
    green = '\033[92m'
    yellow = '\033[93m'
    blue = '\033[94m'
    purple = '\033[95m'
    lightblue = '\033[96m'
    bold = '\033[1m'
    escape = '\033[0m'

def color_text(s, color, bold=True):
    """ Return the given string colored with ANSI codes """
    b = Color.bold if bold else ''
    return Color.escape + b + color + s + Color.escape


def find_modules(workspace):
    """ Get all modules in a given workspace (names + full paths)"""

    modules_paths = {} # key = name, value = path
    for root_dir, subdirs, files in os.walk(workspace):
        # Find the dir '.git' to check if root_dir is a git repo
        if os.path.isdir(os.path.join(root_dir,".git")):
            modules_paths[os.path.basename(os.path.normpath(root_dir))] = root_dir
    return modules_paths


def find_dep_files(workspace):
    """ Find all ".lsimodules" files in a given workspace """
    dep_files = []
    for root_dir, subdirs, files in os.walk(workspace):
        # Find the file '.lsimodules' to get the dependencies
        if os.path.isfile(os.path.join(root_dir,".lsimodules")):
            dep_files.append(os.path.join(root_dir, ".lsimodules"))
    return dep_files    


def parse_url(url):
    """ Get the module's name from the git url """

    # Remove the trailing '.git'
    url = url.replace(".git", "")
    # Find the last '/'
    start = url.rfind('/')
    if start == -1:
        print color_text("Invalid URL", Color.red) + ": {}".format(url)
        exit(1)
    # Return the substring with the package name
    return url[start+1:]


def ssh_to_https(url):
    """ Convert a SSH url to the HTTPS format """
    """ 
        SSH formats:
            ssh://git@bitbucket.org/<accountname>/<reponame>.git
            git@github.com:<accountname>/<reponame>.git
            git@bitbucket.org:<accountname>/<reponame>.git
            git@<server>:<accountname>/<reponame>.git
        HTTPS formats:
            https://github.com/<accountname>/<reponame>.git
            https://bitbucket.org/<accountname>/<reponame>.git
            https://<server>/<accountname>/<reponame>.git
    """

    if not url.startswith('https://') and not url.startswith('ssh://') and not url.startswith('git@'):
        print color_text("Invalid URL", Color.red) + ": {}".format(url)
        exit(1)

    if not url.startswith('https'):
        if url.find(':') != -1:
            # URL format: git@<git_server>:<account_name>/<repo_name>.git
            git_server = url.split(':')[0].split('@')[1]
            account_name = url.split(':')[1].split('/')[0]
            repo_name = url.split(':')[1].split('/')[1]
            # Create the equivalent https url
            url = "https://" + git_server + '/' + account_name + '/' + repo_name
        else:
            # URL format: ssh://git@bitbucket.org/<account_name>/<repo_name>.git
            url = url.replace("ssh://git@", "https://")
    return url


def clone_repo(dep):
    """ Clone a repository in the modules path. """
    if not "path" in dep:
        dep["path"] = ""

    # Get name and full path
    submodule_name = parse_url(dep["url"])
    submodule_path = os.path.join(MODULES_PATH, dep["path"])

    print "Downloading submodule: [{s}] in {p}...".format(s=color_text(submodule_name, Color.lightblue), 
                                                          p=color_text(submodule_path, Color.bold))
    # Create the directory if necessary
    if not os.path.isdir(submodule_path):
        os.makedirs(submodule_path)

    # Clone the repo in the given path
    repo_path = os.path.join(submodule_path, submodule_name)
    clone_cmd = "git clone {url}".format(url=dep["url"])
    if "branch" in dep: clone_cmd += " -b {branch}".format(branch=dep["branch"])
    clone_cmd += " {dir}".format(dir=repo_path)
    return_val = subprocess.call(clone_cmd, shell=True)

    return return_val

def check_installed_package(pkg):
    return_val = 1
    with open(os.devnull, 'w') as devnull:
        return_val = subprocess.call("dpkg-query -W {}".format(pkg), shell=True, stdout=devnull, stderr=devnull)
    return return_val


def get_submodule(dep, modules_paths):
    """ Recursively download all the dependencies of a module """
    global apt_packages

    if not "url" in dep:
        if "name" in dep:
            # Get the URL from the name
            dep["url"] = "https://bitbucket.org/lsi/" + dep["name"] + ".git"
        elif "apt" in dep:
            # System dependency (apt-get)
            # Add the package to the list to bedownloaded in the end
            print "New system dependency: [{}]".format(color_text(dep["apt"], Color.lightblue))
            if check_installed_package(dep["apt"]):
                apt_packages.append(dep["apt"])
            else:
                print color_text("System dependency ", Color.green) +\
                    "[{}]".format(color_text(dep["apt"], Color.lightblue)) +\
                    color_text(" already installed ", Color.green)
            return
        else:
            print color_text("ERROR: Dependency not specified. Missing tag [url|name|apt]", Color.red)
            return

    # Convert url to HTTPS if the option is given
    if FORCE_HTTPS:
        dep["url"] = ssh_to_https(dep["url"])

    submodule_name = dep["name"] if "name" in dep else parse_url(dep["url"])
    print color_text("-----------------------------------------------", Color.blue)
    print "Processing submodule: [{}]".format(color_text(submodule_name, Color.lightblue))
    
    # Check if the submodule is already in the workspace
    if submodule_name in modules_paths:
        print color_text('Found submodule ', Color.green) +\
            '[{m}]'.format(m=color_text(submodule_name, Color.lightblue)) +\
            color_text(' in ', Color.green) +\
            '"{p}"'.format(p=color_text(modules_paths[submodule_name].replace(WORKSPACE+"/", ""), Color.bold))
        return
    
    # Download submodule and iterate its dependencies
    clone_success = clone_repo(dep)

    # Check for errors when cloning the repo
    if clone_success != 0:
        print color_text("ERROR cloning repository: ", Color.red) +\
            color_text("{}", Color.bold).format(dep["url"])
        return

    # Get the dir where the submodule was downloaded
    repo_path = os.path.join(MODULES_PATH, dep["path"], submodule_name)
    # Add the just downloaded repo to the modules_paths dict
    modules_paths[submodule_name] = repo_path
    # Check if the downloaded submodule has dependencies
    dep_file = os.path.join(repo_path, ".lsimodules")
    if os.path.isfile(dep_file):
        print "Submodule file: {}".format(color_text(dep_file.replace(WORKSPACE+"/", ""), Color.bold))
        # Iterate through the module's dependencies
        print "Checking dependencies..."
        deps = []
        with open(dep_file, 'r') as f:
            deps = [dep for dep in yaml.load_all(f)]
        for dep in deps:
            if dep:
                get_submodule(dep, modules_paths)


def init():
    """ Check all the modules in the workspace and download the dependencies """

    # Packages names and paths found in the workspace
    modules_paths = find_modules(WORKSPACE)
    dep_files = find_dep_files(WORKSPACE)

    # For each package, find all dependencies recursively
    for dep_file in dep_files:
        print color_text("##########", Color.blue)
        print "Processing module: [{}]".format(color_text(os.path.basename(os.path.dirname(dep_file)), Color.lightblue))
        print 'Module file: "{}"'.format(color_text(dep_file.replace(WORKSPACE+"/", ""), Color.bold))
        deps = []
        with open(dep_file, 'r') as f:
            deps = [dep for dep in yaml.load_all(f)]
        # Iterate through the module's dependencies
        print "Checking dependencies..."
        for dep in deps:
            if dep:
                get_submodule(dep, modules_paths)

    # Finally, install with apt-get all system dependencies found (all together)
    if len(apt_packages) > 0:
        apt_get_cmd = "sudo apt-get install "
        print "Installing system dependencies:"
        for apt_pkg in apt_packages:
            print "\t{}".format(color_text(apt_pkg, Color.bold))
            apt_get_cmd += apt_pkg + " "
        subprocess.call(apt_get_cmd, shell=True)

def get_branch(repo_path):
    branch = ""
    # First add the current branch
    cd_cmd = "cd {} && ".format(os.path.abspath(repo_path))
    branch = subprocess.check_output(cd_cmd + "git symbolic-ref --short -q HEAD", shell=True).rstrip()
    if branch:
        branch = "On branch\t\t[{}]".format(color_text(branch, Color.green))
    else:
        branch += color_text("HEAD detached (not on any branch)", Color.yellow)
    return branch

def get_status(repo_path):
    """ Get the repository status and return the changes in colored format """
    cd_cmd = "cd {} && ".format(os.path.abspath(repo_path))
    status = subprocess.check_output(cd_cmd + "git status --porcelain", shell=True)
    if status != "":
        # Changes without commit. Do not update.
        changes = status.rstrip().split('\n')
        status_msg = ""
        for c in changes:
            status_msg += "\t" + color_text(c.split()[0], Color.red)
            for s in c.split()[1:]: status_msg += " " + s
            status_msg += '\n'
        status = status_msg
    return status


def update():
    """ Update all repositories (pull) in MODULES_PATH """
    print "Updating submodules..."

    # Find all modules in modules path (not the whole workspace)
    modules_paths = find_modules(MODULES_PATH)
    updated_dep_files = []
    for repo in sorted(modules_paths.keys()):
        print color_text("##########", Color.blue)
        print 'Updating submodule [{}]...'.format(color_text(repo, Color.lightblue))
        cd_cmd = "cd {} && ".format(os.path.abspath(modules_paths[repo]))

        # Check changes in submodule
        if not FORCE_PULL:
            status = get_status(os.path.abspath(modules_paths[repo]))
            if status != "":
                # Changes without commit. Do not update.
                print color_text("WARNING: Changes without commit:", Color.yellow)
                print status
                print 'Skipping submodule [{}]...'.format(color_text(repo, Color.lightblue))
                continue
        # Update the repo
        # Get the current branch to fetch it
        branch = subprocess.check_output(cd_cmd + "git symbolic-ref --short -q HEAD", shell=True).rstrip()
        # Pull
        if branch:
            subprocess.call(cd_cmd + "git pull origin " + branch, shell=True)
        else:
            print color_text("WARNING: HEAD in detached mode", Color.yellow)
            print 'Skipping submodule [{}]...'.format(color_text(repo, Color.lightblue))
    # Check if there are any new dependencies after updating submodules
    if FORCE_REINIT:
        print color_text("##########", Color.green)
        print "Done!"
        print color_text("##########", Color.blue)
        print "Re-Checking dependencies..."
        init()

def status():
    """ Check all repositories in the workspace show the changes """

    # Packages names and paths found in the workspace
    modules_paths = find_modules(WORKSPACE)
    for repo in sorted(modules_paths.keys()):
        print color_text("##########", Color.blue)
        print 'Checking submodule\t[{}]...'.format(color_text(repo, Color.lightblue))
        branch = get_branch(os.path.abspath(modules_paths[repo]))
        print branch
        status = get_status(os.path.abspath(modules_paths[repo]))
        if status != "":
            # Changes without commit. Do not update.
            print color_text("WARNING: Changes without commit:", Color.yellow)
            print status


def parse_args():
    """ Parse input arguments """
    global WORKSPACE; global MODULES_PATH; global FORCE_PULL; global FORCE_REINIT; global FORCE_HTTPS;

    VERSION = "LSI dependencies manager. Version 0.1"
    DESCRIPTION = "LSI dependencies manager."
    USAGE = "lsi_dep <command> [-w <workspace>] [-p <modules_path>] [-v] [-h]"
    HELP = dedent(
        """
        usage: lsi_dep <command> [<options>]

        LSI dependencies manager.

        commands:
          init                 Check and download dependencies
          update               Update submodules repositories
          status               Check the changes in the repositories

        options:
          -w, --workspace <src_path>        Packages workspace
                                            (Default: {w})
          -p, --modules-path <modules_path> Path to install submodules
                                            (Default: {p})
          -f, --force-pull                  Update repositories without checking changes
          -i, --init                        Force a re-init after the update to check for new dependencies
          --https                           Convert all repositories URLs to HTTPS
          -v, --version                     Print version and exit
          -h, --help                        Print this text and exit
    """).format(w=WORKSPACE.replace(os.path.expanduser("~"), "~"),
                p=MODULES_PATH.replace(os.path.expanduser("~"), "~"))

    parser = ArgumentParser(description=DESCRIPTION, usage=USAGE, add_help=False)
    parser.add_argument('command', metavar='<command>', nargs='?', default=None, choices=["init", "update", "status"])
    parser.add_argument('-w', '--workspace', metavar='<src_path>')
    parser.add_argument('-p', '--modules-path', metavar='<modules_path>')
    parser.add_argument('-f', '--force-pull', action="store_true")
    parser.add_argument('-i', '--init', action="store_true")
    parser.add_argument('--https', action="store_true")
    parser.add_argument('-v', '--version', action='version', version=VERSION)
    parser.add_argument('-h', '--help', action='store_true')
    
    args = parser.parse_args()

    # Print help and exit
    if args.help:
        print HELP
        exit()

    # Check if command is given
    if not args.command:
        print color_text("Missing <command> argument", Color.red)
        parser.print_usage()
        print dedent(
        """ 
            commands:
              init                 Check and download dependencies
              update               Update submodules repositories
        """)
        exit(1)

    # Update globals
    if args.workspace:
        WORKSPACE = os.path.abspath(os.path.expanduser(os.path.normpath(args.workspace)))
    else:
        WORKSPACE = os.getcwd()

    if args.modules_path:
        MODULES_PATH = os.path.abspath(os.path.expanduser(os.path.normpath(args.modules_path)))
    else:
        MODULES_PATH = os.path.join(WORKSPACE, "modules")
    
    if not args.modules_path and args.command =="update":
        MODULES_PATH = os.getcwd()

    if args.force_pull:
        FORCE_PULL = True
    if args.init:
        FORCE_REINIT = True
    if args.https:
        FORCE_HTTPS = True

    return args


if __name__ == '__main__':
    # Get aruments
    args = parse_args()

    # Execute command
    if args.command == "init":
        init()
    elif args.command == "update":
        update()
    elif args.command == "status":
        status()

    print color_text("##########", Color.green)
    print "Done!"